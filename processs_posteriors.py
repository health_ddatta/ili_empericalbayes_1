import pandas
import os
import numpy as np
import generate_curve
import utils
import scipy
import math
from scipy.stats import norm
import heapq
import random
import cPickle
import pprint

pp = pprint.PrettyPrinter (indent=4)


# Input :
# pred_list  : list of n 52 week values. [ n x 52 ]
# region
# curr_week cw | cw+1,2,3,4 will be returned
# Returns : next 4 week prediction
# { cw+1 : { 0.1: p1 , 0.2:p2 ... 12:pk},  cw+2: {} ... cw+4:{} }

def get_4wk_predictions(pred_list, curr_ew_week):
    pred_arr = np.asarray (pred_list)
    # print pred_arr.shape

    means = np.mean (pred_arr, axis=0)
    vars = np.var (pred_arr, axis=0)
    # print 'means , vars'
    # print means
    # print vars

    locs = means.tolist ()
    vars = vars.tolist ()

    scales = [math.sqrt (x / utils.max_trials) for x in vars]
    # scales = [math.sqrt (x / 1) for x in vars]
    # scales = list (vars)
    dist_list = []

    for m, s in zip (locs, scales):
        distribution = scipy.stats.norm (loc=m, scale=s)
        # print 'Dist : {', m, ' ~ ', s, '}'
        dist_list.append (distribution)

    bins = np.arange (0.1, 13.1, 0.1).tolist ()

    target_weeks = [curr_ew_week + i for i in range (1, 4 + 1)]
    # print ' - -> curr_ew_week', curr_ew_week
    # print ' - -> target_weeks ', target_weeks

    pred_dict = {}

    # last bin is 13-100
    for target_week in target_weeks:
        threshold = 13
        sum_vals = 0
        map = {}

        for i in bins:
            k_1 = round (i, 2)
            k_2 = round (i + 0.1, 2)

            if (k_1 < threshold):
                map[k_1] = dist_list[target_week].cdf (k_2) - dist_list[target_week].cdf (k_1)
            else:
                map[k_1] = 1 - dist_list[target_week].cdf (threshold)
            sum_vals += map[k_1]
        # pp.pprint (map)
        pred_dict[target_week] = map
    return pred_dict


# Return :
# { week : prob_value , ....}
# week - 1 ... 52
def get_onset_week_predictions(pred_arr, region):
    baseline = utils.get_curr_season_baslines (region)
    # print 'baseline',baseline

    # get the list of weeks
    week_list = []

    for pred_list in pred_arr:

        len_pred_list = len (pred_list)
        shifted_0 = list(pred_list)
        shifted_1 = (list (pred_list))[1:len_pred_list]
        shifted_2 = (list (pred_list))[2:len_pred_list]
        week = 1


        for i, j, k in zip (shifted_0, shifted_1, shifted_2):
            if i >= baseline and j >= baseline and k >= baseline:
                break
            week += 1

        if week < 3 or week > 48:
            week = random.randint (35, 45)
        week_list.append (week)

    # create a normal distribution from these week values
    mean = np.mean (week_list, axis=0)
    _var = np.var (week_list, axis=0)

    # scale = math.sqrt (float (_var) / utils.max_trials)

    # Forceful addition of variation
    scale = max (_var, 0.85)

    dist = scipy.stats.norm (loc=mean, scale=scale)
    bins = range (1, utils.MAX_WEEKS)
    pred_dict = {}

    for b in bins:
        pred_dict[b] = dist.cdf (b + 1) - dist.cdf (b)

    # pp.pprint(pred_dict)
    return pred_dict;


# Return :
# { week : prob_value , ....}
# week - 1 ... 52
def get_peak_perc_predictions(pred_arr):
    value_list = []
    k = 1
    for pred_list in pred_arr:
        max_k = heapq.nlargest (k, pred_list)
        value_list.extend (max_k)

    # create a normal distribution from these week values
    mean = np.mean (value_list, axis=0)

    variance = np.var (value_list, axis=0)
    variance = max(variance,0.5)
    # scale = math.sqrt (variance / utils.max_trials)
    scale = variance
    dist = scipy.stats.norm (loc=mean, scale=scale)
    bins = np.arange (0.1, 13.1, 0.1).tolist ()
    threshold = 13
    pred_dict = {}
    sum_vals = 0

    for i in bins:
        k_1 = round (i, 2)
        k_2 = round (i + 0.1, 2)

        if (k_1 < threshold):
            pred_dict[k_1] = dist.cdf (k_2) - dist.cdf (k_1)
        else:
            pred_dict[k_1] = 1 - dist.cdf (threshold)
        sum_vals += pred_dict[k_1]

    # pp.pprint (pred_dict)
    return pred_dict;


def get_peak_week_predictions(pred_arr):
    week_list = []

    for pred_list in pred_arr:
        max_value = max (pred_list)
        max_index = pred_list.index (max_value)
        week_list.append (max_index + 1)

    mean = np.mean (week_list, axis=0)
    variance = np.var (week_list, axis=0)
    variance = max(variance,0.8)
    scale = variance

    # scale = math.sqrt (variance / utils.max_trials)
    dist = scipy.stats.norm (loc=mean, scale = scale)
    bins = range (1, utils.MAX_WEEKS)
    pred_dict = {}

    for b in bins:
        pred_dict[b] = dist.cdf (b + 1) - dist.cdf (b)

    return pred_dict


# --------------------------------------- #
# Assimilate all predictions into dicts s that can be sent to result writer

def get_all_predictions(curr_ew_week, region="nat"):
    # print 'curr_ew_week',curr_ew_week
    # print 'region',region
    # print ' ---- '

    # TODO : Uncomment these for testing only !!!!
    # generated = 1
    # if generated == 0:
    #     pred_list = generate_curve.run_trials (region, curr_ew_week)
    #     # ----- #
    #     cPickle.dump (pred_list, open ('save.p', 'wb'))
    #
    # else:
    #     pred_list = cPickle.load (open ('save.p', 'rb'))

    # TODO : Uncomment these in real life !!!!
    pred_list = generate_curve.run_trials (region, curr_ew_week)

    four_wk_predictions = get_4wk_predictions (pred_list, curr_ew_week)
    onset_week_predictions = get_onset_week_predictions (pred_list, region)
    peak_week_predictions = get_peak_week_predictions (pred_list)
    peak_perc_predictions = get_peak_perc_predictions (pred_list)

    # adjust the week values in maps

    week_adjusted_4wk_predictions = {}
    for target_week, value_dict in four_wk_predictions.iteritems ():

        week_val = target_week + utils.week_value_adjustment
        if week_val > 52:
            week_val = week_val - 52
        week_adjusted_4wk_predictions[week_val] = value_dict

    week_adjusted_onset_week_predictions = {}

    for target_week, value in onset_week_predictions.iteritems ():
        week_val = target_week + utils.week_value_adjustment
        if week_val > 52:
            week_val = week_val - 52
        week_adjusted_onset_week_predictions[week_val] = value

    week_adjusted_peak_week_predictions = {}

    for target_week, value in peak_week_predictions.iteritems ():
        week_val = target_week + utils.week_value_adjustment
        if week_val > 52:
            week_val = week_val - 52
        week_adjusted_peak_week_predictions[week_val] = value

    return week_adjusted_4wk_predictions, week_adjusted_onset_week_predictions, week_adjusted_peak_week_predictions, peak_perc_predictions


def test():
    curr_ew_week = 35
    region = 'nat'
    get_all_predictions (curr_ew_week, region)
    return

# test()

