import os

# Constants
MAX_YEAR_NO_PRESEASON_DATA = 2002
PRESEASON_START = 21
SEASON_START = 40
END_EW = 20
MIN_WEEKS = 1
MAX_WEEKS = 52
min_year = 1997
max_year = 2017
start_ew = 20
max_ew_curr_yr = 32
errors_file = 'prior_errors.csv'
peaks_file = 'prior_peaks.csv'
current_year = 2017
max_prior_samples = 1500
max_trials = 50
max_proc = 25
week_value_adjustment = 20
op_dir = 'output'

# input : integer y1
# output : string of the form 'y1-y1+1'
def get_years_string(year):
    return str (year) + "-" + str (year + 1);


def get_season(year):
    return str (year) + '-' + str (year + 1);


def get_file_name(year, region, dir=None):
    year_str = get_years_string (int (year))

    if dir is None:
        return year_str + '_' + region + '.csv'
    dir = 'Data/' + dir

    if not os.path.exists (dir):
        os.makedirs (dir);
    return dir + '/' + year_str + '_' + region + '.csv'


def get_region_code(region=1):
    return 'hhs' + str (region);


def get_regions_list():
    reg = []
    reg.append ("nat")
    for i in range (1, 11):
        reg.append (get_region_code (i))
    return reg


def week_in_preaseason(week):
    if week >= PRESEASON_START and week < SEASON_START:
        return True
    return False


def get_curr_season_baslines(region):
    baselines = \
        {"nat": 2.2,
         "hhs1": 1.4,
         "hhs2": 3.1,
         "hhs3": 2,
         "hhs4": 1.9,
         "hhs5": 1.8,
         "hhs6": 4.2,
         "hhs7": 1.9,
         "hhs8": 1.3,
         "hhs9": 2.4,
         "hhs10": 1.4
         }

    return baselines[region]

def get_op_file_name(region,curr_ew_week):
    adj_curr_ew_week = curr_ew_week
    f = 'op_'+str(adj_curr_ew_week) + '_' + region+'.csv'
    return op_dir + '/' + f
