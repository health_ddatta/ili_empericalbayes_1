import schema
import utils
import pandas as pd
import random
from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import math
from random import randint, choice
from multiprocessing import Process
from multiprocessing import Queue
from joblib import Parallel, delayed


# Function to return :
# a curve
# a standard dev(chosen uniformly from past)

def get_single_curve(current_year, region):
    # select the curve
    hist_years = range (utils.min_year, current_year)
    y = choice (hist_years)
    dir = 'Priors' + '/' + region
    file = utils.get_file_name (y, region, dir)
    df_prior = pd.read_csv (file, index_col=None)

    # select the noise
    file = 'Data/Priors/' + region + '/prior_noise.csv'
    df_noise = pd.read_csv (file)
    err_list = df_noise['noise']
    sd = math.sqrt (random.choice (err_list))

    # select the peak value
    # select the peak week
    file = 'Data/Priors/' + region + '/prior_peaks.csv'
    df_peaks = pd.read_csv (file)

    min_peak_wili_hist = df_peaks['peak_value'].min ()
    max_peak_wili_hist = df_peaks['peak_value'].max ()
    peak_wili = random.uniform (min_peak_wili_hist, max_peak_wili_hist)

    lower_bound = df_peaks['peak_week'].min ()
    upper_bound = df_peaks['peak_week'].max ()
    peak_ew = randint (lower_bound, upper_bound)

    # select the pace/scale
    pace = random.uniform (0.75, 1.25)
    baseline = utils.get_curr_season_baslines (region)
    
    prior_peak_wili = df_prior.wili.max ();
    index = ((df_prior.loc[df_prior['wili'] == prior_peak_wili]).index.tolist ())[0]
    prior_peak_week = df_prior.loc[index, 'ew']
    prior_peak_wili = df_prior['wili'].max ()
    ew_list = range (1, utils.MAX_WEEKS + 1)

    df = pd.DataFrame (columns=schema.gen_curve_columns)

    for i in ew_list:

        # from prior get the peak week
        w = i - peak_ew  # if (i-peak_ew) > 0 else i
        w = w / pace
        w = round (w + prior_peak_week)
        # sanity check!

        if (w < 1 or w > 52):
            w = i

        idx = (df_prior.loc[df_prior['ew'] == w].index.tolist ())[0]
        wili_i = df_prior.loc[idx, 'wili']
        wili_i = wili_i - baseline

        wili_i = ((peak_wili - baseline) / (prior_peak_wili - baseline)) * wili_i + baseline
        df = df.append ({'year': current_year, 'ew': i, 'wili': wili_i}, ignore_index=True)

    df = df.reset_index (drop=True)
    return df, sd


#   Inputs:
#   1. wili till current week
#   2. region
#   3. curr_year,
#   4. curr_ew_week
#   5. method
#       1 -> Importance sampling as in paper
#       2 -> weight = 1/ MSE error
def get_curve_aux(curr_df, region, curr_year, curr_ew_week, method=1):
    list_curve = []
    list_wt = []
    max_iterations = utils.max_prior_samples

    for i in range (max_iterations):
        wt = 1
        prior, sd = get_single_curve (curr_year, region)

        if method == 1:

            for i in range (1, curr_ew_week + 1):
                idx = (prior.loc[prior['ew'] == i].index.tolist ())[0]
                prior_curve_pt = prior.loc[idx, 'wili']
                mean = curr_df.loc[i - 1].wili
                # current point
                norm_dist = norm (mean, sd)
                pdf = norm_dist.pdf (prior_curve_pt)

                if pdf != 0: wt *= pdf

        elif method == 2:
            mse = 0
            #print 'curr df',curr_df
            for i in range (1, curr_ew_week + 1):
                idx = (prior.loc[prior['ew'] == i].index.tolist ())[0]
                prior_curve_pt = prior.loc[idx, 'wili']
                curr_curve_pt = curr_df.loc[i - 1].wili
                mse += math.pow (curr_curve_pt - prior_curve_pt, 2)
            mse = math.sqrt (mse)

            wt = 1 / math.pow (mse, 1.5)

        # for weeks previous to & upto curr_week
        # replace the prior values with current values
        # that forms a candidate curve
        candidate = pd.DataFrame (curr_df, index=None)
        tmp = prior[(prior.ew > curr_ew_week)]
        candidate = candidate.append (tmp, ignore_index=True)

        list_curve.append (candidate);
        list_wt.append (wt);
    return list_curve, list_wt


# Input :
# curr_year is start of the current season year
# region
# curr_week
def get_curves(curr_year, region, curr_ew_week):
    #  Get the current(target) year data
    dir = 'Raw' + '/' + region
    file = utils.get_file_name (curr_year, region, dir)
    curr_df = pd.read_csv (file, usecols=schema.curve_columns)

    # slice the dataframe till current week
    # add utils.END_EW ( 20) ,since raw gives actual week( 42, instead of 22 ) num in ew
    curr_df = curr_df.loc[(curr_df.ew <= curr_ew_week + utils.END_EW)]
    list_curve, list_wt = get_curve_aux (curr_df, region, curr_year, curr_ew_week, 2)
    return list_curve, list_wt


def calculate_posterior_mean(curr_year, region, curr_ew_week):
    list_df, list_wt = get_curves (curr_year, region, curr_ew_week)
    print (' Start : calculate_posterior_mean ')

    total_wt = sum (list_wt)
    #   normalize weights
    list_wt = [wt / total_wt for wt in list_wt]

    cutoff_percentile = random.randint (85, 90)
    #   Trying out a cutoff
    cutoff = np.percentile (list_wt, cutoff_percentile)
    total_wt = 0
    for x in list_wt:
        if x >= cutoff:  total_wt += x

    # adjust cutoff
    cutoff = cutoff / total_wt
    list_wt = [wt / total_wt for wt in list_wt]

    list_ew = range (1, utils.MAX_WEEKS + 1)
    # for each ew calculate point
    y = []
    for i in list_ew:
        s = 0
        curve_idx = 0
        for df in list_df:
            wt = list_wt[curve_idx]
            curve_idx += 1
            # discarding by cutoff
            if wt < cutoff: continue
            s = s + df.loc[i - 1, 'wili'] * wt;

        mean = s
        y.append (mean)
    print y
    print (' End : calculate_posterior_mean ')
    
    return y


def plot(curr_ew_week):
    fig = plt.figure ()
    plt.grid ()
    region = "nat"
    curr_year = utils.current_year

    y = calculate_posterior_mean (curr_year, region, curr_ew_week)
    x = range (1, utils.MAX_WEEKS + 1)

    plt.plot (x, y, 'r-')
    file = utils.get_file_name (curr_year, region, 'Priors')
    df = pd.read_csv (file, usecols=['ew', 'wili'])
    y_actual = df['wili'].tolist ()

    plt.plot (x, y_actual, 'b.')
    plt.xlabel ('x')
    plt.ylabel ('y')
    plt.title (' Curve fitting , given data till ew : ' + str (curr_ew_week))
    red = mpatches.Patch (color='red', label='Predicted data points')
    blue = mpatches.Patch (color='blue', label='Real data points')
    plt.legend (handles=[red, blue])
    file_name = 'sim_ewData_' + str (curr_ew_week)
    plt.savefig (file_name)
    # plt.show ()
    plt.close (fig)


def disp_list(list_wili, curr_ew_week):
    fig = plt.figure ()
    plt.grid ()
    region = "nat"
    curr_year = utils.current_year

    dir = 'Raw/' + region
    file = utils.get_file_name (curr_year, region, dir)
    df = pd.read_csv (file, usecols=['ew', 'wili'])
    y_actual = df['wili'].tolist ()
    print y_actual
    # pad Y!
    pad_len = utils.MAX_WEEKS - len (y_actual)
    for _ in range (pad_len):
        y_actual.append (0)

    x = range (1, utils.MAX_WEEKS + 1)
    print y_actual
    print x
    plt.plot (x, y_actual, 'r-')

    for y in list_wili:
        print y
        plt.plot (x, y, 'b.')
        plt.xlabel ('x')
        plt.ylabel ('y')
        plt.title (' Curve fitting , given data till ew : ' + str (curr_ew_week))
        red = mpatches.Patch (color='blue', label='Predicted data points')
        blue = mpatches.Patch (color='red', label='Real data points')
        plt.legend (handles=[red, blue])

    file_name = 'sim_ewData_trial_' + str (curr_ew_week)
    plt.savefig (file_name)
    # plt.show ()
    plt.close (fig)
    return


#def fetch_posterior_mean(curr_year, region, curr_ew_week, output_Q):
#    y = calculate_posterior_mean (curr_year, region, curr_ew_week)
#    output_Q.put (y)
#

def run_trials(region, curr_ew_week):
    list_wili = []
    curr_year = utils.current_year

    max_trials = utils.max_trials
    max_proc = utils.max_proc
    print ('Starting trials , with '+ str(max_proc) + 'processes')

    list_wili = Parallel (n_jobs=max_proc) (
        delayed (calculate_posterior_mean) (curr_year, region, curr_ew_week) for _ in range (max_trials))

    return list_wili


# region = "nat"
# curr_ew_week = 22
# run_trials (region, curr_ew_week)

# def run_trials(region, curr_ew_week):
#
#     list_wili = []
#     curr_year = utils.current_year
#
#     max_trials = utils.max_trials
#     max_proc = utils.max_proc
#     epochs = int(math.ceil (float (max_trials) / max_proc))
#
#     if max_trials % max_proc != 0:
#         utils.max_trials = epochs * max_proc
#         max_trials = utils.max_trials
#
#     for _ in range(epochs) :
#
#         pool = []
#         output_Q = Queue ()
#         for _ in range (max_proc):
#             p = Process (target=fetch_posterior_mean, args=(curr_year, region, curr_ew_week, output_Q,))
#             pool.append (p)
#
#         for p in pool:
#             p.start ()
#
#         for p in pool:
#             p.join ()
#
#         for _ in range (max_trials):
#             z = output_Q.get ()
#             list_wili.append (z)
#
#
#     print list_wili
#     return list_wili

# for curr_ew_week in [20]:
#     plot(curr_ew_week)

# region = "nat"
# curr_ew_week = 22
# run_trials (region, curr_ew_week)

# df,sd = get_single_curve(2016, "nat")
# print df
