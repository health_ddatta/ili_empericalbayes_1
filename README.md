Epidemic season weeks :

-	Preseason Week 21( year y ) - Week 39 ( year y )
-	Week 40 ( year y ) - Week 20 ( year y +1  )



--------------------------------------------------

http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004382
Data can be downloaded from :
https://gis.cdc.gov/GRASP/Fluview/FluHospRates.html

--------------------------------------------------


Preferred:
API endpoint
    https://github.com/cmu-delphi/delphi-epidata#influenza-data
example:
    http://delphi.midas.cs.cmu.edu/epidata/api.php?source=fluview&regions=nat&epiweeks=201530


Params : <br/>
source=fluview <br/>
regions=nat <br/>

List of regions : nat hhs1 hhs2 hhs3 hhs4 hhs5 hhs6 hhs7 hhs8 hhs9 hhs10 cen1 cen2 cen3 cen4 cen5 cen6 cen7 cen8 cen9

epiweeks=201530 <br/> Format : YYYYWW | [201440,201501-201510]


Previously, but now defunct :
Curve Estimation done using : http://contrib.scikit-learn.org/py-earth/content.html#multivariate-adaptive-regression-splines
3

----------------------------------------------------

Run instructions :

python fetch_data.py
Open : vim run_all.sh
increase week by 1 in all the lines.
save ,exit and execute
keep the week value in mind, say previously it was 30, now its 31.
once all the processes are complete,run :

python fetch_output.py
The output folder should have 4Sight_WeekNumber.csv
This week number should be 31+20%52 = 51. if earlier week number was 36 , this week number would be 4. Earlier is a season week, this one is the actual year week.

Run this output with R:
RScript test_cdc.R  <file>

Log in to :
https://predict.phiresearchlab.org/app#/login
ddatta@vt.edu
4sightflu_1

Goto FluSight 2017-18
Upload csv




