import pandas as pd
import utils
import schema
import math
import os


# run the R script
os.system ("Rscript trendfilter.R")

def get_data_from_R(region):
    dir = "/var/tmp/"
    file_name = dir + "R_op_" + region + ".txt"
    f = open (file_name, 'r')

    dict = {}
    while True:
        # read the delimiter
        content = f.readline ()
        content.strip ()
        if "--" in content:
            season_yr = f.readline ()
            season_yr = season_yr.rstrip ('\n')

            yr = f.readline ()
            yr = yr.rstrip ('\n')

            wks = f.readline ()
            wks = wks.rstrip ('\n')

            vals = f.readline ()
            vals = vals.rstrip ('\n')

            yr_dict = {'year': yr.split (","), 'ew': wks.split (","), 'wili': vals.split (",")}

            dict[season_yr] = yr_dict
        else:
            break;
    return dict


def create_filtered_curves(region="nat"):
    data = get_data_from_R (region)
    columns = ['year', 'ew', 'wili']

    for key, value in data.iteritems ():

        yr = int (key)
        season = str (yr) + "-" + str (yr + 1)
        df = pd.DataFrame (columns=columns)
        dir = 'Priors' + '/' + region

        for a, b, c in zip (value['year'], value['ew'], value['wili']):
            df = df.append ({'year': a, 'ew': b, 'wili': c}, ignore_index=True)
        file = utils.get_file_name (yr, region, dir)
        df.to_csv (file, index=False)


def get_shifted_ew(ew):
    if ew < utils.PRESEASON_START:
        return ew + 32
    else:
        return ew - 20


# Fill the weeks prior to 40, where data is absent - with value of first observed value
def process_priors(region="nat"):
    dir = 'Priors' + "/" + region
    years = range ( utils.min_year, utils.max_year )

    def set_ew(row):
        return get_shifted_ew (row['ew'])

    for year in years:
        file = utils.get_file_name (year, region, dir)
        df = pd.read_csv (file, index_col=None)

        if len (df) <= 52:
            tmp = df.head (1)
            wili = tmp.wili[0]
            yr = tmp.year[0]
            fill_weeks = range (utils.PRESEASON_START, int (tmp.ew[0]))
            for fw in fill_weeks:
                dict = {'year': yr, 'ew': fw, 'wili': wili}
                df = df.append (dict, ignore_index=True)
            df = df.sort_values (by=['year', 'ew'], ascending=[True, True])

        # Change the ew to relative values 1 ... 52
        # Store ew s in 'actual_ew'
        df['actual_ew'] = df['ew']
        df['ew'] = df.apply (set_ew, axis=1)
        df.to_csv (file, index=False)

    return;


def calculate_noise(region):
    years = range (utils.min_year, utils.max_year )
    dir1 = 'Raw' + '/' + region
    dir2 = 'Priors' + '/' + region
    dict = {}

    def diff_sq(row):
        return math.pow (row['wili_x'] - row['wili_y'], 2)

    for year in years:
        file1 = utils.get_file_name (year, region, dir1)
        df1 = pd.read_csv (file1, usecols=['year', 'ew', 'wili'])

        file2 = utils.get_file_name (year, region, dir2)
        df2 = pd.read_csv (file2)

        df3 = pd.merge (df1, df2, how='inner', on=['year', 'ew'], left_on=None, right_on=None,
                        left_index=False, right_index=False)
        df3['diff'] = df3.apply (diff_sq, axis=1)
        #   Average of squared errors
        noise = df3['diff'].sum () / len (df3)
        season = utils.get_season (year)
        dict[season] = noise

    # write to file
    file = './Data/Priors/'+ region + '/'+ 'prior_noise.csv'
    df = pd.DataFrame (columns=['season', 'noise'])
    for k, v in dict.iteritems ():
        df = df.append ({'season': k, 'noise': v}, ignore_index=True)
    df.to_csv (file, index=False)

    return;


# This method  calculates peak weak and peak values.
def get_peaks(region="nat"):
    years = range (utils.min_year, utils.max_year)
    peaks = pd.DataFrame (columns=schema.peaks_schema)
    dir = 'Priors' + '/' + region

    for year in years:
        file = utils.get_file_name (year, region, dir)
        df_temp = pd.read_csv (file, index_col=None)
        df_temp = df_temp.reset_index ()
        peak_value = df_temp.wili.max ();
        index = ((df_temp.loc[df_temp['wili'] == peak_value]).index.tolist ())[0]
        peak_week = df_temp.loc[index, 'ew']
        peak_week = get_shifted_ew (peak_week)
        dict = {'season': utils.get_season (year),
                'peak_value': peak_value,
                'peak_week': peak_week}
        peaks = peaks.append (dict, ignore_index=True)

    file = 'Data/Priors/' + region + '/prior_peaks.csv'
    peaks.to_csv (file, index=False, mode='w')
    return;


# ---- Execute above functions ----#
all_regions = utils.get_regions_list ()


for region in all_regions:
    create_filtered_curves (region)
    # first calculate noise, then fill the preseason values
    calculate_noise (region)
    get_peaks (region)
    process_priors (region)
