import result_writer
import utils
import argparse
import pandas as pd
from joblib import Parallel, delayed
import datetime
parser = argparse.ArgumentParser ()

# Example run
# python fetch_output.py -curr_ew k -region hhs1 &
# this value k is essentially the week (starting from 1 corr to actual week 21)
# and at a date of processing (pref Saturday) is the (current week count -1) , here
# again current week is calculated from start of season.

parser.add_argument ('-curr_ew',
                     type=int,
                     help='Enter Current ew week ( starting 1 ,ending 52). at date: 11-01-2017 , it should be 22')
parser.add_argument('-region',
		     type=str,
		     help =" [nat, hhs1 ...10] ")

parser.print_help ()
args = parser.parse_args ()
curr_ew_week = args.curr_ew
region = args.region

# This is a 2017 specific code
# Todo : CHeck the weeks calculation!!!

print 'Curr ew week', curr_ew_week
# ----- #

all_regions = utils.get_regions_list ()
result_writer.write_output_for_region (region, curr_ew_week) 

