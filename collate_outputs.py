import pandas as pd
import utils


curr_ew_week = 35

all_regions = utils.get_regions_list()
# join all the outputs
df = None
for region in all_regions:
    file_name = utils.get_op_file_name(region,curr_ew_week)
    tmp_df = pd.read_csv(file_name,index_col=None)
    if df is None:
        df = tmp_df
    else:
        df = df.append(tmp_df)
        # df = df.reset_index()


adj_curr_ew_week = (curr_ew_week + 20)%52
final_op = utils.op_dir + '/' + '4Sight_' + str(adj_curr_ew_week)+'.csv'
df.to_csv(final_op,index=False)



