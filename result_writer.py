import pandas as pd
import utils
import processs_posteriors


def setup_locationkeys():
    locationkeys = {
        "nat": "US National"
    }

    for i in range (1, 11, 1):
        key = utils.get_region_code (i)
        locationkeys[key] = "HHS Region " + str (i)

    return locationkeys


location_keys = setup_locationkeys ()


# ------------------------------------------------ #

def write_output_for_region(region, curr_ew_week):
    template_file_name = 'ili_template.csv'
    df = pd.read_csv (template_file_name)
    four_wk_predictions, onset_week_predictions, peak_week_predictions, peak_perc_predictions = processs_posteriors.get_all_predictions (
        curr_ew_week, region)

    columns = ["Location", "Target", "Unit", "Bin_start_incl", "Bin_end_notincl", "Value"]

    onset_str = "Season onset"
    peak_week_str = "Season peak week"
    one_wk_ahead_str = "1 wk ahead"
    two_wk_ahead_str = "2 wk ahead"
    three_wk_ahead_str = "3 wk ahead"
    four_wk_ahead_str = "4 wk ahead"
    peak_perc_str = "Season peak percentage"
    week_str = "week"

    region_str = location_keys[region]
    df = df.loc[df['Location'] == region_str]

    adj_curr_ew_week = (curr_ew_week + utils.week_value_adjustment) % 52

    # write the onset week predictions
    for week, value in onset_week_predictions.iteritems ():
        print week, value
        if utils.week_in_preaseason (week): continue

        idx = df.loc[(df['Location'] == region_str) &
                     (df['Target'] == onset_str) &
                     (df['Bin_start_incl'] == str (week))
                     ].index.tolist ()[0]
        df.set_value (idx, 'Value', value)

    # write the peak weeks
    for week, value in peak_week_predictions.iteritems ():

        if utils.week_in_preaseason (week):
            continue

        idx = df.loc[(df['Location'] == region_str) &
                     (df['Target'] == peak_week_str) &
                     (df['Bin_start_incl'] == str (week))
                     ].index.tolist ()[0]
        df.set_value (idx, 'Value', value)

    # write season peak percentages
    for bin_start, value in peak_perc_predictions.iteritems ():
        bin = ('%.3f' % bin_start).rstrip ('0').rstrip ('.')
        idx = df.loc[(df['Location'] == region_str) &
                     (df['Target'] == peak_perc_str) &
                     (df['Bin_start_incl'] == bin)
                     ].index.tolist ()[0]
        df.set_value (idx, 'Value', value)

    # write the 4 weeks ahead prediction
    for week, value_map in four_wk_predictions.iteritems ():
        print 'week', week
        # if utils.week_in_preaseason (week):
        #     continue
        if week == adj_curr_ew_week + 1:
            target_str = one_wk_ahead_str
        elif week == adj_curr_ew_week + 2:
            target_str = two_wk_ahead_str
        elif week == adj_curr_ew_week + 3:
            target_str = three_wk_ahead_str
        elif week == adj_curr_ew_week + 4:
            target_str = four_wk_ahead_str
        else:
            continue
        # print ' Value map ', value_map

        for bin_start, value in value_map.iteritems ():
            bin = ('%.3f' % bin_start).rstrip ('0').rstrip ('.')

            idx = df.loc[(df['Location'] == region_str) &
                         (df['Target'] == target_str) &
                         (df['Bin_start_incl'] == bin)
                         ].index.tolist ()[0]

            df.set_value (idx, 'Value', value)

    output_file = utils.get_op_file_name (region, curr_ew_week)
    df.to_csv (output_file, index=False)
