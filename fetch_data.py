import requests
import utils
import schema
import pandas as pd
import random
import os
import urllib


# Usage :
#   region :  national or one of the hhs regions (See Readme )
#   year : Single year  , int  ; start of season
#   weeks : list of weeks, or a range or a single week
#           | int week | range string w1-wn | list[w1,w2..wn]

def get_api(region, year, weeks):
    host = "http://delphi.midas.cs.cmu.edu/epidata/api.php?"
    src = "fluview"
    params = {}
    params["fmt"] = "json"
    params["source"] = src
    params["regions"] = region
    multi_year = False

    # Fetch data for entire season
    if weeks is None:
        multi_year = True
        start_week = 1
        if year <= utils.MAX_YEAR_NO_PRESEASON_DATA :
            start_week = utils.SEASON_START
        else :
            start_week = utils.PRESEASON_START

        weeks_yr0 = range( start_week, utils.MAX_WEEKS+1 )
        weeks_yr1 = range( 1 , utils.END_EW + 1)
    #To-Do : fill in other  cases later

    tmp_y = year
    years = [tmp_y, tmp_y + 1]

    #List of epiweeks to be passed in parameter
    ew_list = []
    if multi_year:
        for idx, item in enumerate(weeks_yr0):
            weeks_yr0[idx] = str(item).zfill(2)
        for idx, item in enumerate(weeks_yr1):
            weeks_yr1[idx] = str(item).zfill(2)
        ew_list.extend([str(years[0]) + str(week) for week in weeks_yr0]);
        ew_list.extend([str(years[1]) + str(week) for week in weeks_yr1]);
    else:
        # Ensure that week numbers are in format 01,02....52
        for idx, item in enumerate(weeks):
            weeks[idx] = str(item).zfill(2)
        for year in years:
            ew_list.extend([str(year) + str(week) for week in weeks]);

    params["epiweeks"] = ','.join( str(week) for week in ew_list )
    return host, params

# Make the API call
def get_data(region="nat", years = None, weeks = None):

    host, params = get_api(region, years, weeks);
    try:
        # TODO : Fix the url coding Issue
        # should not make 2 get calls

        req = requests.get(host, params=params)
        url = urllib.unquote ( req.url).decode ('utf8')
        req = requests.get(url)
        print req.url
        if (req.status_code != 200):
            raise Exception('Non 200 http status code!!')
        else :
            return req.json ()
    except:
        print 'Error in API call'
        print 'Non 200 Status code '

    return None

# Parse the json data
def process_data(json_data, output_file):

    columns = list(schema.raw_data_columns)
    age_grp_nums = range(6);
    age_grps = ["age_" + str(a) for a in age_grp_nums]
    columns.extend(age_grps)
    master_df = pd.DataFrame(columns=columns)
    data = json_data['epidata']

    for frame in data:
        dict = {}
        dict['wili'] = frame['wili']
        year_ew = str(frame['epiweek'])
        dict['ew'] = int(year_ew[4:6])
        dict['year'] = int(year_ew[0:4])
        dict['num_ili'] = frame['num_ili']
        dict['num_patients'] = frame['num_patients']
        for x in age_grp_nums:
            key1 = 'age_' + str(x)
            key2 = 'num_age_' + str(x)
            dict[key1] = frame[key2]

        df = pd.DataFrame(dict, columns=columns, index=[0])
        master_df = master_df.append(df, ignore_index=True)

    master_df.to_csv(output_file, mode='w',index=False)

    #   Return if there is 52 weeks of data (True) or not (False)
    if len(master_df.index) < utils.MAX_WEEKS - 1:
        return False;
    return True;

def execute():
    years = range(utils.min_year,utils.max_year+1)
    all_regions = utils.get_regions_list()

    for region in all_regions:
        for year in years:
            dir = 'Raw' + '/' + region
            filename = utils.get_file_name(year,region,dir)
            process_data(get_data(region,year),filename)

execute()

